package io.github.ianw11.syncify.background;

import android.content.Context;

import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Error;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerEvent;
import com.spotify.sdk.android.player.Spotify;
import com.spotify.sdk.android.player.SpotifyPlayer;

import io.github.ianw11.syncify.Log;

/**
 * Created by Ian on 12/5/2015.
 */
public class PlayerWrapper implements ConnectionStateCallback, SpotifyPlayer.NotificationCallback {
    private static final String TAG = "PlayerWrapper";

    // Context is only used to destroy the player
    private final Context mContext;
    private PlayerInitializedListener mInitializedListener;

    // The Spotify Player
    private Player mPlayer = null;

    private SpotifyPlayerListener mListener;

    /*
     * Constructor
     */

    public PlayerWrapper(Context context, PlayerInitializedListener listener) {
        mContext = context;
        mInitializedListener = listener;
    }

    public InitializerHandler getHandler() {
        return new InitializerHandler();
    }

    protected void setPlayerListener(SpotifyPlayerListener listener) {
        mListener = listener;
    }

    protected void destroy() {
        Spotify.destroyPlayer(mContext);
        mPlayer = null;
        mListener = null;
    }

    /////////////////////
    // PLAYER CONTROLS //
    /////////////////////

    protected void playSongAtTime(final String songURI, int timeInMS, PlaySongErrorCallback callback) {
        mPlayer.playUri(new Player.OperationCallback() {
            @Override
            public void onSuccess() { }

            @Override
            public void onError(Error error) {
                Log.e("ianTag", "playSongAtTime ERROR: " + error);
                callback.error();
            }
        }, songURI, 0, timeInMS);
    }

    protected boolean getIsPlaying() {
        return mPlayer.getPlaybackState().isPlaying;
    }

    protected void pause() {
        if (!getIsPlaying()) {
            return;
        }
        mPlayer.pause(new Player.OperationCallback() {
            @Override
            public void onSuccess() { }

            @Override
            public void onError(Error error) {
                Log.e("ianTag", "Pause ERROR: " + error);
            }
        });
    }

    ///////////////////////////
    // NOTIFICATION CALLBACK //
    ///////////////////////////

    @Override
    public void onPlaybackEvent(PlayerEvent event) {
        switch (event) {
            case kSpPlaybackNotifyTrackChanged:
                if (mListener != null) {
                    if (mPlayer.getMetadata().currentTrack == null) {
                        // TODO: Do something
                    } else {
                        mListener.onSpotifySongChange();
                    }
                }
                break;
            case kSpPlaybackNotifyPause:
                if (mListener != null) {
                    mListener.onSpotifySongPaused();
                }
                break;
            case kSpPlaybackNotifyPlay:
                if (mListener != null) {
                    if (mPlayer.getMetadata() == null || mPlayer.getMetadata().currentTrack == null) {
                        // TODO: Do something
                    } else {
                        mListener.onSpotifySongPlay();
                    }
                }
                break;
            case kSpPlaybackNotifyAudioDeliveryDone:
                if (mListener != null) {
                    mListener.onSpotifySongEnded();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onPlaybackError(Error error) {
        Log.e(TAG, "onPlaybackError: " + error.toString());
        mListener.onSpotifySongError();
    }

    ///////////////////////////////
    // CONNECTION STATE CALLBACK //
    ///////////////////////////////

    @Override
    public void onLoggedIn() { }

    @Override
    public void onLoggedOut() {
        Log.e(TAG, "onLoggedOut");
    }

    @Override
    public void onLoginFailed(Error error) { }

    @Override
    public void onTemporaryError() { }

    @Override
    public void onConnectionMessage(String s) { }


    ////////////////////////////////////////////////////

    public interface SpotifyPlayerListener {
        void onSpotifySongEnded();
        void onSpotifySongChange();
        void onSpotifySongPaused();
        void onSpotifySongPlay();
        void onSpotifySongError();
    }

    public interface PlayerInitializedListener {
        void initialized(boolean success);
    }

    public interface PlaySongErrorCallback {
        void error();
    }

    /**
     * The class that receives the Spotify player after a successful login and embeds it here
     */
    private class InitializerHandler implements SpotifyPlayer.InitializationObserver {
        @Override
        public void onInitialized(SpotifyPlayer player) {
            player.addConnectionStateCallback(PlayerWrapper.this);
            player.addNotificationCallback(PlayerWrapper.this);
            mPlayer = player;
            mInitializedListener.initialized(true);
            mInitializedListener = null;
        }

        @Override
        public void onError(Throwable throwable) {
            Log.e("ianTag", "InitializerHandler onError: " + throwable.toString());
            mInitializedListener.initialized(false);
            mInitializedListener = null;
        }
    }
}
