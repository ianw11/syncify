package io.github.ianw11.syncify.musicsession;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import io.github.ianw11.syncify.BaseFragment;
import io.github.ianw11.syncify.Log;
import io.github.ianw11.syncify.R;
import io.github.ianw11.syncify.background.PlayerService;
import io.github.ianw11.syncify.background.PlayerWrapper;
import io.github.ianw11.syncify.initialization.LoadingDialogFragment;
import io.github.ianw11.syncify.models.SessionDataModel;
import io.github.ianw11.syncify.models.Song;

/**
 * Created by Ian on 12/5/2015.
 */
public class MusicSessionFragment extends BaseFragment
        implements PlayerWrapper.SpotifyPlayerListener, PlayerService.SessionEventListener {
    private static final String TAG = "MusicSessionFragment";

    private TextView mSongName;
    private TextView mArtistName;
    private TextView mAddedByName;

    private ImageButton mSkipSongButton;
    private TextView mSkipSongText;

    private ImageButton mSaveSongButton;

    private SongListAdapter mAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        final PlayerService service = getPlayerService();
        service.setSessionEventListener(this);
        service.setSpotifyPlayerEventListener(this);

        getSessionDataModel().setSongListObserver(() -> {
            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        });
        getSessionDataModel().setCurrentSongObserver(this::playCurrentSong);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getPlayerService().removeListeners();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_music_session, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mAdapter = new SongListAdapter(view.getContext(), getSessionDataModel().getSongList());

        mSongName = (TextView) view.findViewById(R.id.music_session_song_name);
        mArtistName = (TextView) view.findViewById(R.id.music_session_artist);
        mArtistName.setText("");
        mAddedByName = (TextView) view.findViewById(R.id.music_session_added_by_name);
        mAddedByName.setText("");

        //view.findViewById(R.id.music_session_add_song).setOnClickListener((v) -> showAddSongDialog());
        view.findViewById(R.id.music_session_add).setOnClickListener((v) -> showAddDialog());
        view.findViewById(R.id.music_session_chat_button).setOnClickListener((v) -> showChatDialog());
        view.findViewById(R.id.music_session_info).setOnClickListener((v) -> showInfoWindow());

        mSkipSongText = (TextView) view.findViewById(R.id.music_session_skip_vote_text);
        mSkipSongButton = (ImageButton) view.findViewById(R.id.music_session_skip_button);
        mSkipSongButton.setOnClickListener((v) -> {
            voteSkip();
        });
        mSkipSongButton.setEnabled(false);

        final ListView songNameListView = (ListView) view.findViewById(R.id.song_name_list_view);
        songNameListView.setAdapter(mAdapter);
        songNameListView.setEmptyView(view.findViewById(android.R.id.empty));
        songNameListView.setOnItemClickListener((adapterView, v, position, l) -> {
            final Song song = (Song) songNameListView.getItemAtPosition(position);
            onSongClicked(song, position);
        });

        mSaveSongButton = (ImageButton) view.findViewById(R.id.music_session_save_song);
        mSaveSongButton.setOnClickListener((v) -> saveSongButtonClicked());
        mSaveSongButton.setEnabled(false);

        playCurrentSong();
    }

    @Override
    public boolean backButtonPressed() {
        endSession();
        return true;
    }

    private void playCurrentSong() {
        final SessionDataModel.CurrentSong song = getSessionDataModel().getCurrentSong();
        if (song == null) {
            return;
        }
        getPlayerService().playSongAtTime(song.song.spotifyUri, song.startedAtMillis, () -> {
            Toast.makeText(getContext(), "Error playing song", Toast.LENGTH_SHORT).show();
            if (getSessionDataModel().isSessionHost()) {
                getPlayerService().forceNextSong();
            } else {
                voteSkip();
            }
        });
        setAddedByName(song.addedByName);
        setSongNameText(song.song.name);
        setArtistNameText(song.song.artistNames);

        mSkipSongButton.setVisibility(View.VISIBLE);
        mSkipSongText.setVisibility(View.GONE);

        resetSaveSongButton();
    }

    private void voteSkip() {
        getPlayerService().voteSkip();
        mSkipSongButton.setVisibility(View.GONE);
        mSkipSongText.setVisibility(View.VISIBLE);
        mSkipSongText.setText("Skip vote registered");
    }

    private void endSession() {
        setSongNameText(null);
        setArtistNameText(null);
        getPlayerService().pause();
        getPlayerService().leaveSession();
    }

    //////////////////////
    // USER INTERACTION //
    //////////////////////

    private void onSongClicked(Song song, int offset) {
        // TODO: Add context menu
    }

    private void saveSongButtonClicked() {
        mSaveSongButton.setEnabled(false);
        mSaveSongButton.setImageResource(R.drawable.heart_red_icon);

        getPlayerService().saveSongToSyncifyPlaylist(getSessionDataModel().getCurrentSong().song.spotifyUri, (result) -> {
            if (result != null) {
                Toast.makeText(getContext(), "Unable to save song - try again later", Toast.LENGTH_SHORT).show();
                resetSaveSongButton();
            }
        });
    }

    /////////////////////
    // UI MANIPULATION //
    /////////////////////

    private void setSongNameText(String songName) {
        songName = (songName == null) ? "" : songName;
        mSongName.setText(songName);
        getPlayerService().setNotificationSongTitle(songName);
    }

    private void setArtistNameText(List<String> artistNames) {
        final StringBuilder sb = new StringBuilder();
        if (artistNames != null) {
            for (final String artist : artistNames) {
                sb.append(artist);
                sb.append(", ");
            }
            if (artistNames.size() > 0) {
                sb.deleteCharAt(sb.length() - 1);
                sb.deleteCharAt(sb.length() - 1);
            }
        }
        mArtistName.setText(sb.toString());
        getPlayerService().setNotificationArtistName(sb.toString());
    }

    private void setAddedByName(String name) {
        mAddedByName.setText("Added by " + name);
    }

    private void clearAddedByName() {
        mAddedByName.setText("");
    }

    private void setSongIsPlaying(boolean isPlaying) {
        mSkipSongButton.setEnabled(isPlaying);
    }

    private void showAddDialog() {
        AddItemDialog.newInstance().show(getActivity().getSupportFragmentManager(), null);
    }

    private void showChatDialog() {
        Toast.makeText(getContext(), "Not yet implemented", Toast.LENGTH_SHORT).show();
    }

    private void showInfoWindow() {
        new RoomInfoDialog().show(getActivity().getSupportFragmentManager(), null);
    }

    private void resetSaveSongButton() {
        mSaveSongButton.setEnabled(true);
        mSaveSongButton.setImageResource(R.drawable.heart_icon);
    }

    ////////////////////////////
    // SESSION EVENT LISTENER //
    ////////////////////////////

    @Override
    public void onServiceDestroy() {
        endSession();
    }

    ///////////////////////////////////
    // SPOTIFY PLAYER EVENT LISTENER //
    ///////////////////////////////////


    @Override
    public void onSpotifySongEnded() {
        if (getSessionDataModel().isSessionHost()) {
            getPlayerService().forceNextSong();
        }
        clearNameAndArtist();
    }

    @Override
    public void onSpotifySongChange() {
        setSongIsPlaying(true);
    }

    @Override
    public void onSpotifySongPaused() {
        setSongIsPlaying(false);
    }

    @Override
    public void onSpotifySongPlay() {
        setSongIsPlaying(true);
    }

    @Override
    public void onSpotifySongError() {
        clearNameAndArtist();
        setSongIsPlaying(false);
        if (getSessionDataModel().isSessionHost()) {
            getPlayerService().forceNextSong();
        } else {
            voteSkip();
        }
    }

    private void clearNameAndArtist() {
        setSongNameText("Please wait for next song");
        setArtistNameText(null);
        clearAddedByName();
    }
}
