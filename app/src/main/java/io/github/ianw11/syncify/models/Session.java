package io.github.ianw11.syncify.models;

import com.google.gson.JsonObject;

import java.io.Serializable;

/**
 * Created by Ian on 2/21/2017.
 */

public class Session implements Serializable {
    public final String id;
    public final String name;
    public final String creatorName;
    public int numInRoom;

    public Session(JsonObject obj) {
        id = obj.get("id").getAsString();
        name = obj.get("name").getAsString();
        creatorName = obj.get("creatorName").getAsString();
        numInRoom = obj.get("numUsers").getAsInt();
    }

    public void updateNumInRoom(int newVal) {
        numInRoom = newVal;
    }
}
