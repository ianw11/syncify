package io.github.ianw11.syncify.musicsession;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.github.ianw11.syncify.Log;
import io.github.ianw11.syncify.R;
import io.github.ianw11.syncify.SyncifyApplication;
import io.github.ianw11.syncify.models.Playlist;
import io.github.ianw11.syncify.models.Song;
import io.github.ianw11.syncify.utils.ViewUtils;

/**
 * Created by Ian on 2/19/2017.
 */

public class AddPlaylistDialog extends DialogFragment {
    private static final String TAG = "AddPlaylistDialog";

    public final static String PLAYLISTS = "playlists";

    public static AddPlaylistDialog newInstance(Playlist[] playlists) {
        final AddPlaylistDialog dialog = new AddPlaylistDialog();
        final Bundle args = new Bundle();
        args.putSerializable(PLAYLISTS, playlists);
        dialog.setArguments(args);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View view = ViewUtils.inflateView(getContext(), R.layout.dialog_add_playlist);

        // Get the playlist parameter
        final Playlist[] playlists = (Playlist[]) getArguments().getSerializable(PLAYLISTS);

        // Get the spinner and apply the adapter
        final Spinner playlistSpinner = (Spinner)view.findViewById(R.id.add_playlist_spinner);
        playlistSpinner.setAdapter(new PlaylistAdapter(getActivity(), Arrays.asList(playlists)));

        final CheckBox shuffleCheckbox = (CheckBox)view.findViewById(R.id.add_playlist_shuffle_checkbox);

        return new AlertDialog.Builder(getContext())
                .setView(view)
                .setPositiveButton("Add playlist", (dialogInterface, n) -> {
                    final Playlist selectedPlaylist = (Playlist)playlistSpinner.getSelectedItem();
                    final SyncifyApplication application = ((SyncifyApplication)getContext().getApplicationContext());
                    application.getPlayerService().getPlaylistTracks(selectedPlaylist, (songArr) -> {
                        if (songArr == null) {
                            Log.e(TAG, "Error loading playlist tracks");
                            return;
                        }

                        final List<Song> songs = new ArrayList<>(Arrays.asList(songArr));
                        if (shuffleCheckbox.isChecked()) {
                            Collections.shuffle(songs);
                        }

                        application.getPlayerService().addSongs(songs);
                    });

                    dialogInterface.dismiss();
                })
                .setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss())
                .create();
    }

    private class PlaylistAdapter extends ArrayAdapter<Playlist> {
        private PlaylistAdapter(Context context, List<Playlist> list) {
            super(context, R.layout.list_item_simple, list);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            final String playlistName = getItem(position).name;

            final TextView view = (TextView) super.getDropDownView(position, convertView, parent);
            view.setText(playlistName);

            // Scale down the text size to prevent overflow
            final int textLength = playlistName.length();
            final int textSize;
            if (textLength > 16) {
                if (textLength < 24) {
                    textSize = 24;
                } else if (textLength < 36) {
                    textSize = 20;
                } else {
                    textSize = 16;
                }
            } else {
                textSize = 28;
            }
            // A little bit of optimization
            if (view.getTag() == null || textSize != (int)view.getTag()) {
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
                view.setTag(textSize);
            }

            return view;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            final Playlist playlist = getItem(position);

            if (convertView == null) {
                convertView = ViewUtils.inflateViewIntoParent(getContext(), R.layout.list_item_simple, parent);
            }

            ((TextView)convertView).setText(playlist.name);

            return convertView;
        }
    }
}
