package io.github.ianw11.syncify.roomselection;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import io.github.ianw11.syncify.R;
import io.github.ianw11.syncify.SyncifyApplication;
import io.github.ianw11.syncify.models.Song;

/**
 * Created by Ian on 12/5/2015.
 */
public class CreateRoomDialog extends DialogFragment {

    private static final int MAX_ROOM_NAME_CHARS = 25;

    public static CreateRoomDialog newInstance() {
        final CreateRoomDialog dialog = new CreateRoomDialog();
        final Bundle args = new Bundle();
        dialog.setArguments(args);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.create_room_popup_window, container, false);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setTitle("Create a session");

        view.findViewById(R.id.create_room_cancel_button).setOnClickListener((v) -> dismiss());
        final Button submitButton = (Button) view.findViewById(R.id.create_room_submit_button);

        final TextView charactersRemaining = (TextView) view.findViewById(R.id.create_room_characters_remaining);
        charactersRemaining.setText(MAX_ROOM_NAME_CHARS + " characters remaining");

        // Get the EditText that will hold the new Room name
        final EditText sessionNameEditText = (EditText) view.findViewById(R.id.popup_edit_text);
        sessionNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final int length = editable.length();
                if (length > MAX_ROOM_NAME_CHARS) {
                    submitButton.setEnabled(false);
                    charactersRemaining.setText("Name too long, please shorten");
                } else {
                    submitButton.setEnabled(true);
                    charactersRemaining.setText((MAX_ROOM_NAME_CHARS - length) + " characters remaining");
                }
            }
        });

        submitButton.setOnClickListener((v) -> attemptSubmit(sessionNameEditText.getText().toString()));

        return view;
    }

    private void attemptSubmit(String sessionName) {
        if (TextUtils.isEmpty(sessionName)) {
            Toast.makeText(getContext(), "Enter a name for the session", Toast.LENGTH_SHORT).show();
            return;
        }

        final SyncifyApplication application = ((SyncifyApplication) getContext().getApplicationContext());
        application.getPlayerService().createSession(sessionName);

        dismiss();
    }
}
