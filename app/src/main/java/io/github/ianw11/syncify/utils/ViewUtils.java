package io.github.ianw11.syncify.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Ian on 12/5/2015.
 */
public class ViewUtils {

    public static View inflateView(Context context, int viewId) {
        return LayoutInflater.from(context).inflate(viewId, null);
    }
    
    public static View inflateViewIntoParent(Context context, int viewId, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(viewId, parent, false);
    }
}
