package io.github.ianw11.syncify.initialization;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import io.github.ianw11.syncify.BaseFragment;
import io.github.ianw11.syncify.R;

/**
 * Created by Ian on 2/20/2017.
 */

public class ClosedWebsocketFragment extends BaseFragment {

    private ClosedWebsocketFragmentListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ClosedWebsocketFragmentListener) {
            mListener = (ClosedWebsocketFragmentListener) context;
        } else {
            throw new IllegalStateException(context.getClass().getSimpleName() + " must implement ClosedWebsocketFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_websocket_closed, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        final Button reconnectButton = (Button) view.findViewById(R.id.reconnect_websocket_button);
        reconnectButton.setEnabled(true);

        reconnectButton.setOnClickListener((v) -> {
            reconnectButton.setEnabled(false);
            if (mListener == null) {
                return;
            }

            getPlayerService().newWebsocketConnection((success) -> {
                if (mListener != null) {
                    mListener.websocketRecreated(success);
                }
            }, () -> reconnectButton.setEnabled(true));
        });
    }

    public interface ClosedWebsocketFragmentListener {
        void websocketRecreated(boolean success);
    }
}
