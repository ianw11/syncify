package io.github.ianw11.syncify.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ian on 2/18/2017.
 */

public class SessionDataModel {

    private final List<Session> mSessions = new ArrayList<>();
    private SessionObserver mSessionObserver;

    private Session mCurrentSession = null;

    private boolean mIsSessionHost = false;

    private final List<Song> mSongList = new ArrayList<>();
    private SongListObserver mSongListObserver = null;

    private CurrentSong mCurrentSong = null;
    private CurrentSongObserver mCurrentSongObserver = null;

    public void clearMusicSession() {
        setIsSessionHost(false);
        setCurrentSession(null);
        clearSongs();
        mCurrentSong = null;
        clearMusicSessionObservers();
    }

    public void setSessions(List<Session> sessions) {
        mSessions.clear();
        if (sessions != null) {
            mSessions.addAll(sessions);
        }
        notifySessionObserver();
    }

    public List<Session> getSessions() {
        return mSessions;
    }

    public void setCurrentSession(Session session) {
        mCurrentSession = session;
    }

    public Session getCurrentSession() {
        return mCurrentSession;
    }

    public void setIsSessionHost(boolean isHost) {
        mIsSessionHost = isHost;
    }

    public boolean isSessionHost() {
        return mIsSessionHost;
    }

    public void setSongs(List<CurrentSong> songs) {
        mSongList.clear();
        for (final CurrentSong song : songs) {
            mSongList.add(song.song);
        }
        notifySongListObserver();
    }

    public void clearSongs() {
        mSongList.clear();
        notifySongListObserver();
    }

    public List<Song> getSongList() {
        return mSongList;
    }

    public void setCurrentSong(CurrentSong song) {
        mCurrentSong = song;
        notifyCurrentSongObserver();
    }

    public CurrentSong getCurrentSong() {
        return mCurrentSong;
    }

    private void clearMusicSessionObservers() {
        setSongListObserver(null);
        setCurrentSongObserver(null);
    }

    public void setSessionObserver(SessionObserver observer) {
        mSessionObserver = observer;
    }

    public void notifySessionObserver() {
        if (mSessionObserver != null) {
            mSessionObserver.sessionsChanged();
        }
    }

    public void setSongListObserver(SongListObserver observer) {
        mSongListObserver = observer;
    }

    private void notifySongListObserver() {
        if (mSongListObserver != null) {
            mSongListObserver.dataSetChanged();
        }
    }

    public void setCurrentSongObserver(CurrentSongObserver observer) {
        mCurrentSongObserver = observer;
    }

    private void notifyCurrentSongObserver() {
        if (mCurrentSongObserver != null) {
            mCurrentSongObserver.newSong();
        }
    }

    public interface SessionObserver {
        void sessionsChanged();
    }

    public interface SongListObserver {
        void dataSetChanged();
    }

    public interface CurrentSongObserver {
        void newSong();
    }

    public static class CurrentSong {
        public final Song song;
        public final int startedAtMillis;
        public final String addedByName;

        public CurrentSong(String songUri, int startedAtMillis, String addedByName, String songName, List<String> artists) {
            this.song = new Song(songUri, songName, artists);
            this.startedAtMillis = startedAtMillis;
            this.addedByName = addedByName;
        }
    }
}
