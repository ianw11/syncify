package io.github.ianw11.syncify.roomselection;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.github.ianw11.syncify.BaseFragment;
import io.github.ianw11.syncify.R;
import io.github.ianw11.syncify.models.Session;
import io.github.ianw11.syncify.utils.ViewUtils;

/**
 * Created by Ian on 12/5/2015.
 */
public class RoomSelectionFragment extends BaseFragment {

    private final List<Session> mFilteredSessionList = new ArrayList<>();
    private SessionAdapter mAdapter;
    private EditText mSearchBar;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        getSessionDataModel().setSessionObserver(this::filterSessions);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getSessionDataModel().setSessionObserver(null);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_room_selection, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        view.findViewById(R.id.select_session_create_room_button).setOnClickListener((v) ->
                CreateRoomDialog.newInstance().show(getActivity().getSupportFragmentManager(), null));

        mAdapter = new SessionAdapter(getContext(), R.layout.list_item_session, mFilteredSessionList);
        final ListView sessionListView = (ListView) view.findViewById(R.id.select_session_list_view);
        sessionListView.setAdapter(mAdapter);
        sessionListView.setOnItemClickListener((adapterView, v, position, l) -> getPlayerService().joinSession(((Session) sessionListView.getItemAtPosition(position)).id));

        final ImageButton clearButton = (ImageButton)view.findViewById(R.id.select_session_search_clear_button);
        clearButton.setVisibility(View.GONE);

        mSearchBar = (EditText)view.findViewById(R.id.select_session_search_bar);
        mSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                clearButton.setVisibility(editable.length() > 0 ? View.VISIBLE : View.GONE);
                filterSessions();
            }
        });

        clearButton.setOnClickListener((v) -> mSearchBar.setText(""));

        filterSessions();
    }

    private void filterSessions() {
        final String filter = mSearchBar.getText().toString();
        mFilteredSessionList.clear();
        for (final Session session : getSessionDataModel().getSessions()) {
            if (session.name.contains(filter) || session.creatorName.contains(filter) || session.id.contains(filter.toUpperCase())) {
                mFilteredSessionList.add(session);
            }
        }

        mAdapter.notifyDataSetChanged();
    }

    private class SessionAdapter extends ArrayAdapter<Session> {
        @LayoutRes
        private final int mLayoutId;

        private SessionAdapter(Context context, @LayoutRes int layoutId, List<Session> list) {
            super(context, layoutId, list);
            mLayoutId = layoutId;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final Session session = getItem(position);

            final ViewHolder holder;
            if (convertView == null) {
                convertView = ViewUtils.inflateViewIntoParent(getContext(), mLayoutId, parent);
                holder = new ViewHolder((TextView)convertView.findViewById(R.id.list_item_session_name),
                        (TextView)convertView.findViewById(R.id.list_item_owner_name),
                        (TextView)convertView.findViewById(R.id.session_num_users),
                        (TextView)convertView.findViewById(R.id.session_id));
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder)convertView.getTag();
            }

            holder.sessionName.setText(session.name);
            holder.creatorName.setText(session.creatorName);
            holder.numUsers.setText(Integer.toString(session.numInRoom));
            //holder.sessionId.setText(session.id);

            return convertView;
        }

        private class ViewHolder {
            final TextView sessionName;
            final TextView creatorName;
            final TextView numUsers;
            final TextView sessionId;

            public ViewHolder(TextView sessionName, TextView creatorName, TextView numUsers, TextView sessionId) {
                this.sessionName = sessionName;
                this.creatorName = creatorName;
                this.numUsers = numUsers;
                this.sessionId = sessionId;
            }
        }
    }
}
