package io.github.ianw11.syncify.musicsession;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import io.github.ianw11.syncify.R;
import io.github.ianw11.syncify.models.Song;
import io.github.ianw11.syncify.utils.ViewUtils;

/**
 * Created by Ian on 1/16/2016.
 */
public class SongListAdapter extends ArrayAdapter<Song> {

    @LayoutRes
    private static final int RESOURCE_ID = R.layout.list_item_song;

    private final Context mContext;
    
    public SongListAdapter(Context context, List<Song> list) {
        super(context, RESOURCE_ID, list);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Song song = getItem(position);

        final SongListViewHolder holder;
        if (convertView == null) {
            convertView = ViewUtils.inflateViewIntoParent(mContext, RESOURCE_ID, parent);

            holder = new SongListViewHolder((TextView)convertView.findViewById(R.id.song_list_item_name),
                    (TextView)convertView.findViewById(R.id.song_list_item_artist));
            convertView.setTag(holder);
        } else {
            holder = (SongListViewHolder) convertView.getTag();
        }

        final StringBuilder sb = new StringBuilder();
        for (final String artist : song.artistNames()) {
            sb.append(artist + ", ");
        }
        if (song.numArtists() > 0) {
            sb.deleteCharAt(sb.length() - 1);
            sb.deleteCharAt(sb.length() - 1);
        }
        holder.name.setText(song.name);
        holder.artist.setText(sb.toString());

        return convertView;
    }

    private class SongListViewHolder {
        private final TextView name;
        private final TextView artist;

        private SongListViewHolder(TextView name, TextView artist) {
            this.name = name;
            this.artist = artist;
        }
    }
}
