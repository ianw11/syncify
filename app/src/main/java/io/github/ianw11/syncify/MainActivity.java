package io.github.ianw11.syncify;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.github.ianw11.syncify.background.PlayerService;
import io.github.ianw11.syncify.background.WebsocketWrapper;
import io.github.ianw11.syncify.initialization.ClosedWebsocketFragment;
import io.github.ianw11.syncify.models.Session;
import io.github.ianw11.syncify.models.SessionDataModel;
import io.github.ianw11.syncify.musicsession.MusicSessionFragment;
import io.github.ianw11.syncify.roomselection.RoomSelectionFragment;
import io.github.ianw11.syncify.utils.BackButtonListener;

import static io.github.ianw11.syncify.SyncifyApplication.SPOTIFY_CLIENT_ID;

public class MainActivity extends AppCompatActivity implements ClosedWebsocketFragment.ClosedWebsocketFragmentListener {
    private static final String TAG = "MainActivity";

    private static final int SPOTIFY_REQUEST_CODE = 3217;
    private static final String REDIRECT_URL = "syncify://home";
    private static final String[] SCOPES = {"user-read-private",
            "streaming",
            "playlist-read-private",
            "playlist-modify-private",
            "playlist-modify-public",
    };

    private final Set<BackButtonListener> mBackButtonListeners = new HashSet<>();

    private TextView mLoadingTextView;
    private TextView mErrorTextView;
    private ProgressBar mLoadingSpinner;

    private FragmentState mCurrentState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mLoadingTextView = (TextView)findViewById(R.id.loading_text);
        mLoadingTextView.setText("Loading... please wait");

        mErrorTextView = (TextView)findViewById(R.id.loading_error_text);
        mLoadingSpinner = (ProgressBar)findViewById(R.id.loading_progress_spinner);

        if (savedInstanceState != null) {
            return;
        }

        /*
        Lifecycle order:
            - Connect to background service
                bindWithService()
            - Log in to Spotify
                serviceBound() callback calls openLoginActivity()
            - Initialize Fragments and Controllers with Login credentials
                onActivityResult() -> onSpotifyLogin()
            - Begin normal app flow
                FragmentManager updates with the correct fragment
         */

        getSyncifyApplication().bindWithService(() -> {
            mLoadingTextView.setText("Bound with service, logging into spotify");
            // And log in to Spotify
            AuthenticationClient.openLoginActivity(this, SPOTIFY_REQUEST_CODE,
                    new AuthenticationRequest.Builder(SPOTIFY_CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URL)
                            .setScopes(SCOPES)
                            .setShowDialog(true)
                            .build());
        });
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SPOTIFY_REQUEST_CODE) {
            mLoadingTextView.setText("Logged in with spotify, getting auth token");
            getPlayerService().onSpotifyLogin(resultCode, data, (result) -> {
                if (result == null) {
                    mLoadingTextView.setText("");
                    mLoadingSpinner.setVisibility(View.GONE);

                    connectWebsocket();
                } else {
                    mLoadingTextView.setText("Well this is awkward. This app is cr-app. Please restart the app and report this message:");
                    mErrorTextView.setText(result);
                    mLoadingSpinner.setVisibility(View.GONE);
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        deregisterWebsocketCallbacks();
        super.onDestroy();
        getSyncifyApplication().unbindFromService();
    }

    @Override
    public void onBackPressed() {
        boolean handled = false;
        for (final BackButtonListener listener : mBackButtonListeners) {
            handled |= listener.backButtonPressed();
        }
        if (!handled) {
            super.onBackPressed();
        }
    }

    public void registerBackButtonListener(BackButtonListener listener) {
        mBackButtonListeners.add(listener);
    }

    public void deregisterBackButtonListener(BackButtonListener listener) {
        mBackButtonListeners.remove(listener);
    }

    private void registerWebsocketCallbacks() {
        getPlayerService().registerCallback(WebsocketWrapper.ServerCommand.JOINED_SESSION, (args) -> {
            final Session session = new Session(new JsonParser().parse(args[0]).getAsJsonObject());
            getSyncifyApplication().getSessionDataModel().setCurrentSession(session);
            replaceFragment(FragmentState.MUSIC_SESSION);
            Log.d("ianTag", "Joined session: " + session.id);
        });
        getPlayerService().registerCallback(WebsocketWrapper.ServerCommand.LEFT_SESSION, (args) -> {
            getSyncifyApplication().getSessionDataModel().clearMusicSession();
            replaceFragment(FragmentState.ROOM_SELECTION);
        });
        getPlayerService().registerCallback(WebsocketWrapper.ServerCommand.SESSION_HOST, (args) -> getSyncifyApplication().getSessionDataModel().setIsSessionHost(true));
        getPlayerService().registerCallback(WebsocketWrapper.ServerCommand.SONG_LIST_UPDATE, (args) -> {
            final JsonArray arr = new JsonParser().parse(args[0]).getAsJsonArray();

            final SessionDataModel dataModel = getSyncifyApplication().getSessionDataModel();
            if (arr.size() == 0) {
                dataModel.clearSongs();
                return;
            }

            final List<SessionDataModel.CurrentSong> songs = new ArrayList<>(arr.size());
            for (final JsonElement songElem : arr) {
                final JsonObject obj = songElem.getAsJsonObject();
                final String uri = obj.get("uri").getAsString();
                final String title = obj.get("title").getAsString();
                final JsonArray artists = obj.get("artists").getAsJsonArray();
                final List<String> artistNames = new ArrayList<>(artists.size());
                for (final JsonElement elem : artists) {
                    artistNames.add(elem.getAsString());
                }
                final String addedBy = obj.get("addedByName").getAsString();

                final SessionDataModel.CurrentSong song = new SessionDataModel.CurrentSong(uri, -1, addedBy, title, artistNames);
                songs.add(song);
            }
            dataModel.setSongs(songs);
        });
        getPlayerService().registerCallback(WebsocketWrapper.ServerCommand.NEXT_SONG, (args) -> {
            final JsonObject obj = new JsonParser().parse(args[0]).getAsJsonObject();
            final String uri = obj.get("uri").getAsString();
            final String addedBy = obj.get("addedByName").getAsString();
            final String songTitle = obj.get("title").getAsString();
            final int songMillis = obj.get("millis").getAsInt();
            final JsonArray songArtists = obj.get("artists").getAsJsonArray();
            final List<String> artists = new ArrayList<>();
            for (final JsonElement elem : songArtists) {
                artists.add(elem.getAsString());
            }
            getSyncifyApplication().getSessionDataModel().setCurrentSong(new SessionDataModel.CurrentSong(uri, songMillis, addedBy, songTitle, artists) );
        });

        getPlayerService().registerCallback(WebsocketWrapper.ServerCommand.SESSION_LIST_UPDATE, (args) -> {
            final JsonArray arr = new JsonParser().parse(args[0]).getAsJsonArray();
            final List<Session> sessions = new ArrayList<>();
            for (final JsonElement elem : arr) {
                final JsonObject root = elem.getAsJsonObject();
                final Session session = new Session(root);
                sessions.add(session);
            }

            getSyncifyApplication().getSessionDataModel().setSessions(sessions);
        });

        getPlayerService().registerCallback(WebsocketWrapper.ServerCommand.ERROR, (args) -> {
            final String message = args[0] == null ? "No Message" : args[0];
            Toast.makeText(this, "Server Error: " + message, Toast.LENGTH_SHORT).show();
        });

        getPlayerService().registerCallback(WebsocketWrapper.ServerCommand.SESSION_NUM_PEOPLE_UPDATE, (args) -> {
            if (getSyncifyApplication().getSessionDataModel().getCurrentSession() != null) {
                getSyncifyApplication().getSessionDataModel().getCurrentSession().updateNumInRoom(Integer.parseInt(args[0]));
            }
        });
    }

    private void deregisterWebsocketCallbacks() {
        getPlayerService().deregisterCallback(WebsocketWrapper.ServerCommand.JOINED_SESSION);
        getPlayerService().deregisterCallback(WebsocketWrapper.ServerCommand.LEFT_SESSION);
        getPlayerService().deregisterCallback(WebsocketWrapper.ServerCommand.SESSION_HOST);
        getPlayerService().deregisterCallback(WebsocketWrapper.ServerCommand.SONG_LIST_UPDATE);
        getPlayerService().deregisterCallback(WebsocketWrapper.ServerCommand.NEXT_SONG);
        getPlayerService().deregisterCallback(WebsocketWrapper.ServerCommand.SESSION_LIST_UPDATE);
        getPlayerService().deregisterCallback(WebsocketWrapper.ServerCommand.ERROR);
    }

    ///////////////
    // WEBSOCKET //
    ///////////////

    private void connectWebsocket() {
        getPlayerService().newWebsocketConnection(this::websocketRecreated, this::websocketClosed);
    }

    @Override
    public void websocketRecreated(boolean success) {
        if (success) {
            registerWebsocketCallbacks();
            Log.d("ianTag", "Successful websocket connection");
            replaceFragment(FragmentState.ROOM_SELECTION);
        } else {
            mLoadingTextView.setText("Unable to contact Syncify server.  Please try again.");
        }
    }

    private void websocketClosed() {
        if (!isDestroyed()) {
            replaceFragment(FragmentState.DEAD_SOCKET);
        }
    }

    ///////////////////////
    // APPLICATION-LEVEL //
    ///////////////////////

    private SyncifyApplication getSyncifyApplication() {
        return (SyncifyApplication)getApplication();
    }

    private PlayerService getPlayerService() {
        return getSyncifyApplication().getPlayerService();
    }

    /////////////////////////////
    // SCREEN/STATE MANAGEMENT //
    /////////////////////////////

    private void replaceFragment(FragmentState fragmentState) {
        mCurrentState = fragmentState;
        final Fragment fragment;
        switch (mCurrentState) {
            case ROOM_SELECTION:
                fragment = new RoomSelectionFragment();
                break;
            case MUSIC_SESSION:
                fragment = new MusicSessionFragment();
                break;
            case DEAD_SOCKET:
                fragment = new ClosedWebsocketFragment();
                break;
            default:
                throw new IllegalStateException("Unknown state: " + mCurrentState);
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_fragment_holder, fragment)
                .disallowAddToBackStack()
                .commit();
    }

    private enum FragmentState {
        ROOM_SELECTION,
        MUSIC_SESSION,
        DEAD_SOCKET,
        ;
    }
}
