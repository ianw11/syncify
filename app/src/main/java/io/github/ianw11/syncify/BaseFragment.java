package io.github.ianw11.syncify;

import android.content.Context;
import android.support.v4.app.Fragment;

import io.github.ianw11.syncify.background.PlayerService;
import io.github.ianw11.syncify.models.SessionDataModel;
import io.github.ianw11.syncify.utils.BackButtonListener;

/**
 * Created by Ian on 2/17/2017.
 */

public abstract class BaseFragment extends Fragment implements BackButtonListener {

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof MainActivity) {
            ((MainActivity)context).registerBackButtonListener(this);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (getContext() instanceof MainActivity) {
            ((MainActivity)getContext()).deregisterBackButtonListener(this);
        }
    }

    /*
     * Override this method to be notified when the back button is pressed
     */
    @Override
    public boolean backButtonPressed() {
        return false;
    }

    protected PlayerService getPlayerService() {
        return getSyncifyApplication().getPlayerService();
    }

    protected SyncifyApplication getSyncifyApplication() {
        return (SyncifyApplication) getContext().getApplicationContext();
    }

    protected SessionDataModel getSessionDataModel() {
        return getSyncifyApplication().getSessionDataModel();
    }
}
