package io.github.ianw11.syncify.background;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.github.ianw11.syncify.Log;
import io.github.ianw11.syncify.models.Playlist;
import io.github.ianw11.syncify.models.Song;
import kaaes.spotify.webapi.android.SpotifyCallback;
import kaaes.spotify.webapi.android.SpotifyError;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.ArtistSimple;
import kaaes.spotify.webapi.android.models.Pager;
import kaaes.spotify.webapi.android.models.PlaylistSimple;
import kaaes.spotify.webapi.android.models.PlaylistTrack;
import kaaes.spotify.webapi.android.models.Track;
import kaaes.spotify.webapi.android.models.TracksPager;
import kaaes.spotify.webapi.android.models.UserPrivate;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ian on 12/5/2015.
 */
public class ServiceWrapper {
    private static final String TAG = "ServiceWrapper";

    private static final String SYNCIFY_PLAYLIST_NAME = "Added by Syncify";

    private final SpotifyService mService;
    private UserPrivate mMe;

    private Playlist[] mCachedPlaylists = null;

    public ServiceWrapper(SpotifyService service, ServiceInitializedCallback callback) {
        mService = service;
        mService.getMe(new SpotifyCallback<UserPrivate>() {
            @Override
            public void failure(SpotifyError spotifyError) {
                callback.initialized(false);
            }

            @Override
            public void success(UserPrivate userPrivate, Response response) {
                mMe = userPrivate;
                callback.initialized(true);
            }
        });
    }

    protected void clearCache() {
        mCachedPlaylists = null;
    }

    protected boolean arePlaylistsCached() {
        return mCachedPlaylists != null;
    }

    protected String getCurrentName() {
        if (mMe == null) {
            return null;
        }
        return mMe.display_name;
    }

    protected String getCurrentId() {
        if (mMe == null) {
            return null;
        }
        return mMe.id;
    }

    protected void getPlaylistTracks(final Playlist playlist, final ServiceCallback<Song[]> callback) {
        mService.getPlaylistTracks(playlist.owner, playlist.uri, new Callback<Pager<PlaylistTrack>>() {
            @Override
            public void success(Pager<PlaylistTrack> playlistTrackPager, Response response) {
                // We go to a recursive method to handle paging/multiple requests
                playlistSuccess(playlist, playlistTrackPager, new ArrayList<>(), 100, callback);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("ianTag", "ServiceWrapper getPlaylistTracks INITIAL failure: " + error.toString());
                callback.onResult(null);
            }
        });
    }

    private void playlistSuccess(Playlist playlist, Pager<PlaylistTrack> currentPager, List<PlaylistTrack> tracks, int offset, ServiceCallback<Song[]> callback) {
        tracks.addAll(currentPager.items);

        // If there's a next request
        if (currentPager.next != null) {
            mService.getPlaylistTracks(playlist.owner, playlist.uri, offset, new Callback<Pager<PlaylistTrack>>() {
                @Override
                public void success(Pager<PlaylistTrack> playlistTrackPager, Response response) {
                    playlistSuccess(playlist, playlistTrackPager, tracks, offset + 100, callback);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("ianTag", "ServiceWrapper getPlaylistTracks failure: " + error.toString());
                    callback.onResult(null);
                }
            });
            return;
        }

        final Song[] songs = new Song[tracks.size()];
        for (int i = 0; i < tracks.size(); ++i) {
            final Track track = tracks.get(i).track;
            final ArtistSimple[] artists = new ArtistSimple[track.artists.size()];
            track.artists.toArray(artists);
            songs[i] = new Song(track.uri, track.name, artists);
        }
        callback.onResult(songs);
    }

    protected void getMyPlaylists(final ServiceCallback<Playlist[]> callback) {
        if (mCachedPlaylists != null) {
            callback.onResult(mCachedPlaylists);
            return;
        }

        mService.getPlaylists(getCurrentId(), new Callback<Pager<PlaylistSimple>>() {
            @Override
            public void success(Pager<PlaylistSimple> playlistSimplePager, Response response) {
                mCachedPlaylists = new Playlist[playlistSimplePager.items.size()];
                for (int i = 0; i < playlistSimplePager.items.size(); ++i) {
                    final PlaylistSimple playlist = playlistSimplePager.items.get(i);
                    final String[] split = playlist.uri.split(":");
                    final String uri = split[split.length - 1];
                    mCachedPlaylists[i] = new Playlist(playlist.name, playlist.owner.id, uri);
                }
                callback.onResult(mCachedPlaylists);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("ianTag", "ServiceWrapper getMyPlaylists failure: " + error.toString());
                callback.onResult(null);
            }
        });
    }

    protected void getSongURI(final String songName, final ServiceCallback<String> callback) {
        mService.searchTracks(songName, new Callback<TracksPager>() {
            @Override
            public void success(TracksPager tracksPager, Response response) {
                callback.onResult(tracksPager.tracks.items.get(0).uri);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("ianTag", "ServiceWrapper getSongURI failure: " + error.toString());
                callback.onResult(null);
            }
        });
    }

    protected void searchForSongByName(final String songName, final ServiceCallback<List<Song>> callback) {
        mService.searchTracks(songName, new Callback<TracksPager>() {
            @Override
            public void success(TracksPager tracksPager, Response response) {
                final List<Song> songs = new ArrayList<>();
                for (final Track track : tracksPager.tracks.items) {
                    // Gather all artist names
                    final List<String> artists = new ArrayList<>();
                    for (final ArtistSimple artist : track.artists) {
                        artists.add(artist.name);
                    }
                    // Build the song
                    songs.add(new Song(track.uri, track.name, artists));
                }
                callback.onResult(songs);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("ianTag", "ServiceWrapper searchForSongByName failure: " + error.toString());
                callback.onResult(null);
            }
        });
    }

    protected void getArtists(final String songIdRaw, final ServiceCallback<List<ArtistSimple>> callback) {
        final String[] split = songIdRaw.split(":");
        final String songId = split[split.length - 1];
        mService.getTrack(songId, new Callback<Track>() {
            @Override
            public void success(Track track, Response response) {
                callback.onResult(track.artists);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("ianTag", "ServiceWrapper getArtistName failure: " + error.toString());
                callback.onResult(null);
            }
        });
    }

    protected void saveSongToSyncifyPlaylist(final String songUri, final ServiceCallback<String> callback) {
        getMyPlaylists((myPlaylists) -> {
            Playlist syncifyPlaylist = null;

            // See if the playlist exists already
            for (final Playlist playlist : myPlaylists) {
                if (playlist.name.equals(SYNCIFY_PLAYLIST_NAME)) {
                    syncifyPlaylist = playlist;
                }
            }

            // If the playlist doesn't exist, create it.  Otherwise save the song to it
            if (syncifyPlaylist == null) {
                final Map<String, Object> body = new HashMap<>();
                body.put("name", SYNCIFY_PLAYLIST_NAME);
                mService.createPlaylist(getCurrentId(), body, new Callback<kaaes.spotify.webapi.android.models.Playlist>() {
                    @Override
                    public void success(kaaes.spotify.webapi.android.models.Playlist playlist, Response response) {
                        saveSongToSyncifyPlaylist(songUri, playlist.uri, callback);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e(TAG, "Error creating playlist: " + error.getKind());
                        Log.e(TAG, error.getResponse().getReason());
                        callback.onResult("Unable to create syncify playlist");
                    }
                });
            } else {
                saveSongToSyncifyPlaylist(songUri, syncifyPlaylist.uri, callback);
            }
        });
    }

    private void saveSongToSyncifyPlaylist(String songUri, String playlistUri, final ServiceCallback<String> callback) {
        final Map<String, Object> query = new HashMap<>();
        query.put("uris", songUri);
        mService.addTracksToPlaylist(getCurrentId(), playlistUri, query, new HashMap<>(), new Callback<Pager<PlaylistTrack>>() {
            @Override
            public void success(Pager<PlaylistTrack> playlistTrackPager, Response response) {
                callback.onResult(null);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, "Error saving song to playlist: " + error.getKind());
                callback.onResult("Unable to save song to playlist");
            }
        });
    }

    /**
     * The interface to callback whenever anything requests data.
     */
    public interface ServiceCallback<K> {
        void onResult(K data);
    }

    public interface ServiceInitializedCallback {
        void initialized(boolean success);
    }
}
