package io.github.ianw11.syncify;

import android.app.Application;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import io.github.ianw11.syncify.background.PlayerService;
import io.github.ianw11.syncify.models.SessionDataModel;

/**
 * Created by Ian on 12/5/2015.
 */
public class SyncifyApplication extends Application {
    private static final String TAG = "SyncifyApplication";

    public static final String SPOTIFY_CLIENT_ID = "fe0b3f254fea4fef8a3f8bf3c7c789a3";

    // The Spotify (background) service
    private PlayerService mPlayerService = null;

    // The model that handles callbacks and holds the current state
    private SessionDataModel mSessionDataModel = new SessionDataModel();

    // The bridge between the application and the background service where the media player
    // and other spotify functions exist
    private ServiceConnection mServiceConnection = null;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.initialize(this);
    }

    /////////////////////
    // SERVICE METHODS //
    /////////////////////

    public void bindWithService(ServiceBoundCallback callback) {
        if (mServiceConnection != null) {
            callback.serviceBound();
        }

        // Start service to ensure it exists
        // If already started, this method does nothing
        startService(new Intent(this, PlayerService.class));

        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                // Once the service is bound, retain a reference to it
                mPlayerService = ((PlayerService.PlayerServiceBinder) iBinder).getPlayerService();

                callback.serviceBound();
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                Log.e(TAG, "OnServiceDisconnected.  This is unexpected.  " + componentName);
            }
        };

        // Bind this activity to the service
        bindService(new Intent(this, PlayerService.class), mServiceConnection, 0);
    }

    public void unbindFromService() {
        if (mServiceConnection == null) {
            return;
        }

        // Remove the reference to the service and detach from it
        mPlayerService = null;
        unbindService(mServiceConnection);
        mServiceConnection = null;
    }

    ///////////////
    // APP STATE //
    ///////////////

    public PlayerService getPlayerService() {
        return mPlayerService;
    }

    public SessionDataModel getSessionDataModel() {
        return mSessionDataModel;
    }

    /////////////////////
    // PRIVATE HELPERS //
    /////////////////////

    /**
     * Callback after binding with the background service
     */
    public interface ServiceBoundCallback {
        void serviceBound();
    }
}
