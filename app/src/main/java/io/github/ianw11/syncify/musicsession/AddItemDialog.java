package io.github.ianw11.syncify.musicsession;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;
import android.widget.Button;

import io.github.ianw11.syncify.Log;
import io.github.ianw11.syncify.R;
import io.github.ianw11.syncify.SyncifyApplication;
import io.github.ianw11.syncify.initialization.LoadingDialogFragment;
import io.github.ianw11.syncify.utils.ViewUtils;

/**
 * Created by Ian on 4/17/2017.
 */

public class AddItemDialog extends AppCompatDialogFragment {
    private static final String TAG = "AddItemDialog";

    public static AddItemDialog newInstance() {
        return new AddItemDialog();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View view = ViewUtils.inflateView(getContext(), R.layout.dialog_add_item);
        final AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setView(view)
                .create();

        final Button addSongButton = (Button)view.findViewById(R.id.add_song_button);
        addSongButton.setOnClickListener(button -> {
            AddSongDialog.newInstance().show(getActivity().getSupportFragmentManager(), null);
            dialog.dismiss();
        });

        final Button addPlaylistButton = (Button)view.findViewById(R.id.add_playlist_button);
        addPlaylistButton.setOnClickListener(button -> {
            final LoadingDialogFragment loadingFragment;
            if (!((SyncifyApplication)getActivity().getApplication()).getPlayerService().arePlaylistsCached()) {
                loadingFragment = new LoadingDialogFragment();
                loadingFragment.show(getActivity().getSupportFragmentManager(), null);
            } else {
                loadingFragment = null;
            }

            // Hold a reference to the original activity for when the dialog gets dismissed
            final FragmentActivity activity = getActivity();

            ((SyncifyApplication)getActivity().getApplication()).getPlayerService().getMyPlaylists((playlists) -> {
                if (loadingFragment != null) {
                    loadingFragment.dismiss();
                }

                if (playlists == null) {
                    Log.e(TAG, "Unable to show playlists - error occurred");
                    return;
                }

                AddPlaylistDialog.newInstance(playlists).show(activity.getSupportFragmentManager(), null);
            });

            dialog.dismiss();
        });

        return dialog;
    }
}
