package io.github.ianw11.syncify.utils;

/**
 * Created by Ian on 2/21/2017.
 */

public interface BackButtonListener {
    boolean backButtonPressed();
}
