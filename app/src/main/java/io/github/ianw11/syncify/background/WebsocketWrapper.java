package io.github.ianw11.syncify.background;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.neovisionaries.ws.client.OpeningHandshakeException;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.github.ianw11.syncify.Log;
import io.github.ianw11.syncify.models.Song;

/**
 * Created by Ian on 2/18/2017.
 */

public class WebsocketWrapper {
    private static final String TAG = "WebsocketWrapper";

    private static final String ENDPOINT = "ws://192.168.1.2:8100/syncifysocket";
    //private static final String ENDPOINT =  "ws://nectarsac.com/syncifysocket";

    private static final String DELIM_REGEX = "\\|";
    private static final String DELIM = "|";

    private final WebSocket mSocket;
    private final Map<ServerCommand, WebsocketCallback> mCallbacks = new HashMap<>();

    public WebsocketWrapper(String name, WebsocketInitializedCallback initializedCallback, WebsocketClosedCallback closedCallback) {
        try {
            mSocket = new WebSocketFactory().setConnectionTimeout(5000).createSocket(ENDPOINT);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        mSocket.addListener(new WebSocketAdapter() {
            private int mUnknownCommandCounter = 0;

            @Override
            public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
                runOnUIThread(() -> {
                    initializedCallback.initialized(true);
                    send(Command.NAME, name);
                });
            }

            @Override
            public void onTextMessage(WebSocket websocket, String text) throws Exception {
                final String[] split = text.split(DELIM_REGEX);
                final String commandStr = split[0];
                final ServerCommand command;
                try {
                    command = ServerCommand.toServerCommand(commandStr);
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, "Unknown server command: " + commandStr);
                    if (++mUnknownCommandCounter > 3) {
                        Log.e(TAG, "Too many websocket errors", true);
                        mSocket.disconnect();
                    }
                    return;
                }
                mUnknownCommandCounter = 0;
                android.util.Log.d(TAG, "Received command: " + command);

                if (mCallbacks.containsKey(command)) {
                    final String[] args = Arrays.copyOfRange(split, 1, split.length);
                    runOnUIThread(() -> mCallbacks.get(command).onTrigger(args) );
                } else {
                    Log.e(TAG, "No registered callback for " + command);
                }
            }

            @Override
            public void onDisconnected(WebSocket websocket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) throws Exception {
                runOnUIThread(closedCallback::socketClosed);
            }
        });

        runInBackground(()-> {
            try {
                mSocket.connect();
            } catch (OpeningHandshakeException e) {
                android.util.Log.e(TAG, "OpeningHandshakeException");
                runOnUIThread(() -> initializedCallback.initialized(false));
            } catch (WebSocketException e) {
                android.util.Log.e(TAG, "WebSocketException during connect()");
                runOnUIThread(() -> initializedCallback.initialized(false));
            }
        });
    }

    private void send(Command command, String... args) {
        if (!mSocket.isOpen()) {
            android.util.Log.e("ianTag", "Unable to send websocket data - websocket is closed");
            return;
        }

        final StringBuilder sb = new StringBuilder();
        sb.append(command);
        for (final String arg : args) {
            sb.append(DELIM + arg);
        }
        final String message = sb.toString();
        android.util.Log.d("ianTag", "Websocket sending (" + message.length() + " bytes): " + message);
        runInBackground(() -> mSocket.sendText(message));
    }

    public void terminate() {
        mSocket.disconnect();
        mCallbacks.clear();
    }

    public void registerCallback(ServerCommand command, WebsocketCallback callback) {
        if (mCallbacks.containsKey(command)) {
            Log.e(TAG, "Unable to register " + command + " because the command already exists", false);
            return;
        }

        mCallbacks.put(command, callback);
    }

    public void deregisterCallback(ServerCommand command) {
        if (mCallbacks.containsKey(command)) {
            mCallbacks.remove(command);
        }
    }

    public void sendLog(io.github.ianw11.syncify.Log.LogLevel logLevel, String tag, String message) {
        final JsonObject obj = new JsonObject();
        obj.add("LEVEL", new JsonPrimitive(logLevel.label()));
        obj.add("TAG", new JsonPrimitive(tag));
        obj.add("MESSAGE", new JsonPrimitive(message));
        send(Command.LOG, obj.toString());
    }

    public void createSession(String sessionName) {
        send(Command.CREATE_SESSION, sessionName);
    }

    public void joinSession(String id) {
        send(Command.JOIN_SESSION, id);
    }

    public void leaveSession() {
        send(Command.LEAVE_SESSION);
    }

    public void voteSkip() {
        send(Command.VOTE_SKIP);
    }

    public void forceNextSong() {
        send(Command.NEXT_SONG);
    }

    public void addSongs(List<Song> songs) {
        final JsonArray arr = new JsonArray();
        for (final Song song : songs) {
            arr.add(song.toJson());
        }
        send(Command.ADD_SONGS, arr.toString());
    }

    //////////////////////
    // Background Utils //
    //////////////////////

    private void runInBackground(Runnable runnable) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                runnable.run();
                return null;
            }
        }.execute();
    }

    private void runOnUIThread(Runnable runnable) {
        new Handler(Looper.getMainLooper()).post(runnable);
    }

    //////////////////////////////////////////////////////////

    private enum Command {
        LOG,
        NAME,
        CREATE_SESSION,
        JOIN_SESSION,
        LEAVE_SESSION,
        VOTE_SKIP,
        NEXT_SONG, // If the Host hits a Spotify error
        ADD_SONGS,
        ;
    }

    public enum ServerCommand {
        ERROR,
        SESSION_LIST_UPDATE,
        JOINED_SESSION,
        NEXT_SONG,
        SONG_LIST_UPDATE,
        SESSION_HOST,
        LEFT_SESSION,
        SONG_QUEUE_CAPACITY,
        SESSION_NUM_PEOPLE_UPDATE,
        ;

        static ServerCommand toServerCommand(String command) {
            return ServerCommand.valueOf(command.toUpperCase());
        }
    }

    public interface WebsocketCallback {
        void onTrigger(String... args);
    }

    public interface WebsocketInitializedCallback {
        void initialized(boolean success);
    }

    public interface WebsocketClosedCallback {
        void socketClosed();
    }
}
