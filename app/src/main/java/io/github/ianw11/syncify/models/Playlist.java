package io.github.ianw11.syncify.models;

/**
 * Created by Ian on 2/17/2017.
 */

public class Playlist {
    public final String name;
    public final String owner;
    public final String uri;

    public Playlist(String name, String owner, String uri) {
        this.name = name;
        this.owner = owner;
        this.uri = uri;
    }
}
