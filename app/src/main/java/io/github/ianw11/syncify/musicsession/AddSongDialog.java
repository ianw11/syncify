package io.github.ianw11.syncify.musicsession;

import android.app.Dialog;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import io.github.ianw11.syncify.R;
import io.github.ianw11.syncify.SyncifyApplication;
import io.github.ianw11.syncify.background.PlayerService;
import io.github.ianw11.syncify.background.ServiceWrapper;
import io.github.ianw11.syncify.models.Song;
import io.github.ianw11.syncify.utils.ViewUtils;
import kaaes.spotify.webapi.android.models.ArtistSimple;

/**
 * Created by Ian on 2/19/2017.
 */

public class AddSongDialog extends DialogFragment {
    private static final String TAG = "AddSongDialog";

    public static AddSongDialog newInstance() {
        return new AddSongDialog();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View popup = ViewUtils.inflateView(getContext(), R.layout.dialog_add_song);
        final AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setView(popup)
                /*
                .setPositiveButton("Add song", (dialogInterface, i) -> {
                    final SyncifyApplication application = ((SyncifyApplication)getContext().getApplicationContext());
                    final PlayerService service = application.getPlayerService();

                    final String name = text.getText().toString();
                    if (TextUtils.isEmpty(name)) {
                        return;
                    }

                    service.getSongUri(name, (uri) -> {
                        service.getArtists(uri, (artists) -> {
                            final List<String> artistNames = new ArrayList<>(artists.size());
                            for (final ArtistSimple artist : artists) {
                                artistNames.add(artist.name);
                            }
                            service.addSongs(Collections.singletonList(new Song(uri, name, artistNames)));
                        });
                    });
                })
                */
                .setNegativeButton("Done", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .create();

        final List<Song> songResults = new ArrayList<>();
        final SongSearchResultAdapter adapter = new SongSearchResultAdapter(getContext(), songResults, pos -> {
            final SyncifyApplication application = ((SyncifyApplication)getContext().getApplicationContext());
            final PlayerService service = application.getPlayerService();
            service.addSongs(Collections.singletonList(songResults.get(pos)));
            dialog.dismiss();
        });

        final EditText text = (EditText) popup.findViewById(R.id.add_song_text);

        final Button searchButton = (Button)popup.findViewById(R.id.add_song_search_button);
        searchButton.setOnClickListener(button -> {
            final SyncifyApplication application = ((SyncifyApplication)getContext().getApplicationContext());
            final PlayerService service = application.getPlayerService();
            final String name = text.getText().toString();
            if (TextUtils.isEmpty(name)) {
                return;
            }

            service.searchForSongsByName(name, data -> {
                songResults.clear();
                songResults.addAll(data);
                adapter.notifyDataSetChanged();
            });

        });

        final ListView scrollView = (ListView)popup.findViewById(R.id.add_song_search_result_recyclerview);
        scrollView.setAdapter(adapter);

        return dialog;
    }

    private class SongSearchResultAdapter extends ArrayAdapter<Song> {
        private final SongResultClickedCallback mOnClickCallback;

        public SongSearchResultAdapter(Context context, List<Song> songs, SongResultClickedCallback callback) {
            super(context, R.layout.list_item_song, songs);
            mOnClickCallback = callback;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final Song song = getItem(position);

            if (convertView == null) {
                convertView = ViewUtils.inflateViewIntoParent(getContext(), R.layout.list_item_song, parent);
                convertView.setOnClickListener(view -> {
                    mOnClickCallback.onClick(position);
                });
            }

            ((TextView)convertView.findViewById(R.id.song_list_item_name)).setText(song.name);
            ((TextView)convertView.findViewById(R.id.song_list_item_artist)).setText(song.artistNames.get(0));

            return convertView;
        }
    }

    private interface SongResultClickedCallback {
        void onClick(int position);
    }
}
