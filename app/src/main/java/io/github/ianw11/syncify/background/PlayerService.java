package io.github.ianw11.syncify.background;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.Spotify;

import java.util.List;

import io.github.ianw11.syncify.Log;
import io.github.ianw11.syncify.R;
import io.github.ianw11.syncify.models.Playlist;
import io.github.ianw11.syncify.models.Song;
import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.models.ArtistSimple;

import static io.github.ianw11.syncify.SyncifyApplication.SPOTIFY_CLIENT_ID;

/**
 * Created by Ian on 1/9/2016.
 */
public class PlayerService extends Service {
    private static final int NOTIFICATION_ID = 3217;

    // The number of bound activities
    private int mNumBound = 0;

    // The wrapper to the music player
    private PlayerWrapper mPlayerWrapper;
    // The wrapper to the Spotify service (song/artist lookup)
    private ServiceWrapper mServiceWrapper;
    // The wrapper to the websocket
    private WebsocketWrapper mWebsocketWrapper;

    private SessionEventListener mSessionEventListener;

    // Used for the Notification
    private NotificationManager mNotificationManager;
    private String mSongTitle = "";
    private String mSongArtist = "";


    // CONSTRUCTOR

    public PlayerService() { }

    // LIFECYCLE METHODS

    @Override
    public void onCreate() {
        // Put an icon in the notification bar
        mNotificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        //showNotification();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        ++mNumBound;
        return new PlayerServiceBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        if ((--mNumBound) == 0) {
            onTaskRemoved(intent);
        }
        return true;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        stopSelf();
    }

    @Override
    public void onDestroy() {
        if (mSessionEventListener != null) {
            mSessionEventListener.onServiceDestroy();
        }

        removeListeners();
        removeNotification();
        mNotificationManager = null;
        mServiceWrapper = null;
        if (mPlayerWrapper != null) {
            mPlayerWrapper.destroy();
            mPlayerWrapper = null;
        }
        if (mWebsocketWrapper != null) {
            mWebsocketWrapper.terminate();
            mWebsocketWrapper = null;
        }
    }

    //////////////////////////
    // //////////////////// //
    // // ACTUAL METHODS // //
    // //////////////////// //
    //////////////////////////

    public void onSpotifyLogin(int resultCode, Intent data, SpotifyLoginCallback callback) {
        if (mPlayerWrapper != null) {
            callback.loggedIn(null);
            return;
        }

        final AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, data);
        if (response.getType() == AuthenticationResponse.Type.TOKEN) {
            final String accessToken = response.getAccessToken();

            // Create a wrapper around the API to perform song lookups
            final SpotifyApi api = new SpotifyApi();
            api.setAccessToken(accessToken);
            mServiceWrapper = new ServiceWrapper(api.getService(), (serviceSuccess) -> {
                if (!serviceSuccess) {
                    callback.loggedIn("Error connecting with ServiceWrapper");
                    return;
                }

                // Then create a wrapper around the Spotify player
                mPlayerWrapper = new PlayerWrapper(this, (playerSuccess) -> callback.loggedIn(playerSuccess ? null : "Error connecting to PlayerWrapper"));

                // The handler embeds the resulting player in the wrapper
                final Config playerConfig = new Config(this, accessToken, SPOTIFY_CLIENT_ID);
                Spotify.getPlayer(playerConfig, this, mPlayerWrapper.getHandler());
            });
            return;
        }

        callback.loggedIn("Authentication response is not TOKEN, instead it's: " + response.getType());
    }

    public void newWebsocketConnection(WebsocketWrapper.WebsocketInitializedCallback initializedCallback, WebsocketWrapper.WebsocketClosedCallback closedCallback) throws IllegalStateException {
        if (mWebsocketWrapper != null) {
            mWebsocketWrapper.terminate();
            mWebsocketWrapper = null;
        }

        // This constructor may throw an IllegalStateException if it is unable to connect
        mWebsocketWrapper = new WebsocketWrapper(mServiceWrapper.getCurrentName(), initializedCallback, closedCallback);
    }

    public PlayerWrapper getPlayerWrapper() {
        return mPlayerWrapper;
    }

    public ServiceWrapper getServiceWrapper() {
        return mServiceWrapper;
    }

    public WebsocketWrapper getWebsocketWrapper() {
        return mWebsocketWrapper;
    }

    // EVENT LISTENERS

    public void setSpotifyPlayerEventListener(PlayerWrapper.SpotifyPlayerListener listener) {
        mPlayerWrapper.setPlayerListener(listener);
    }

    public void removeSpotifyPlayerEventListener() {
        if (mPlayerWrapper != null) {
            mPlayerWrapper.setPlayerListener(null);
        }
    }

    public void setSessionEventListener(SessionEventListener listener) {
        mSessionEventListener = listener;
    }

    public void removeSessionEventListener() {
        mSessionEventListener = null;
    }

    public void removeListeners() {
        removeSpotifyPlayerEventListener();
        removeSessionEventListener();
    }

    // NOTIFICATIONS

    private Notification notificationTemplate(String ticker, String title, String text) {
        //PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        return new Notification.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(ticker)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(title)
                .setContentText(text)
                .setOngoing(true)
                //.setContentIntent(pendingIntent)
                .build();
    }

    private void showNotification() {
        //Notification notification = notificationTemplate("Syncify", "Syncify", "");
        //mNotificationManager.notify(NOTIFICATION_ID, notification);
    }

    private void editNotification(Notification notification) {
        //mNotificationManager.notify(NOTIFICATION_ID, notification);
    }

    private void removeNotification() {
        //mNotificationManager.cancel(NOTIFICATION_ID);
    }

    ///////////////////////////////////////////////////
    // PASSTHROUGH FUNCTIONS (FOR DIFFERENT CONTEXT) //
    ///////////////////////////////////////////////////

    public void setNotificationSongTitle(String songTitle) {
        mSongTitle = songTitle;
        editNotification(notificationTemplate("Syncify", "Syncify", mSongTitle + " by " + mSongArtist));
    }

    public void setNotificationArtistName(String artist) {
        mSongArtist = artist;
        editNotification(notificationTemplate("Syncify", "Syncify", mSongTitle + " by " + mSongArtist));
    }

    // SERVICE

    public void clearCache() {
        mServiceWrapper.clearCache();
    }

    public boolean arePlaylistsCached() {
        return mServiceWrapper.arePlaylistsCached();
    }

    public void getMyPlaylists(ServiceWrapper.ServiceCallback<Playlist[]> callback) {
        mServiceWrapper.getMyPlaylists(callback);
    }

    public void getPlaylistTracks(Playlist playlist, ServiceWrapper.ServiceCallback<Song[]> callback) {
        mServiceWrapper.getPlaylistTracks(playlist, callback);
    }

    public void getSongUri(String songName, ServiceWrapper.ServiceCallback<String> callback) {
        mServiceWrapper.getSongURI(songName, callback);
    }

    public void searchForSongsByName(String songName, ServiceWrapper.ServiceCallback<List<Song>> callback) {
        mServiceWrapper.searchForSongByName(songName, callback);
    }

    public void getArtists(String uri, ServiceWrapper.ServiceCallback<List<ArtistSimple>> callback) {
        mServiceWrapper.getArtists(uri, callback);
    }

    public void saveSongToSyncifyPlaylist(String songUri, ServiceWrapper.ServiceCallback<String> callback) {
        mServiceWrapper.saveSongToSyncifyPlaylist(songUri, callback);
    }

    // PLAYER

    public void pause(){
        mPlayerWrapper.pause();
    }

    public void playSongAtTime(String uri, int timeInMillis, PlayerWrapper.PlaySongErrorCallback callback) {
        mPlayerWrapper.playSongAtTime(uri, timeInMillis, callback);
    }

    public boolean getIsPlaying() {
        return mPlayerWrapper.getIsPlaying();
    }

    // WEBSOCKET

    public void registerCallback(WebsocketWrapper.ServerCommand command, WebsocketWrapper.WebsocketCallback callback) {
        mWebsocketWrapper.registerCallback(command, callback);
    }

    public void deregisterCallback(WebsocketWrapper.ServerCommand command) {
        // Deregister needs to be verified, in case the OS did something weird lifecycle-wise
        if (mWebsocketWrapper != null) {
            mWebsocketWrapper.deregisterCallback(command);
        }
    }

    public void sendLog(Log.LogLevel logLevel, String tag, String message) {
        if (mWebsocketWrapper == null) {
            android.util.Log.e("ianTag", "mSocketWrapper is not yet initialized.  Message: " + message);
        } else {
            mWebsocketWrapper.sendLog(logLevel, tag, message);
        }
    }

    public void createSession(String sessionName) {
        mWebsocketWrapper.createSession(sessionName);
    }

    public void joinSession(String sessionId) {
        mWebsocketWrapper.joinSession(sessionId);
    }

    public void leaveSession() {
        mWebsocketWrapper.leaveSession();
    }

    public void voteSkip() {
        mWebsocketWrapper.voteSkip();
    }

    public void forceNextSong() {
        mWebsocketWrapper.forceNextSong();
    }

    public void addSongs(List<Song> songs) {
        mWebsocketWrapper.addSongs(songs);
    }

    /**
     * This binder is used to pass this PlayerService back to the main activity
     */
    public class PlayerServiceBinder extends Binder {
        public PlayerService getPlayerService() {
            return PlayerService.this;
        }
    }

    public interface SessionEventListener {
        void onServiceDestroy();
    }

    public interface SpotifyLoginCallback {
        void loggedIn(String message);
    }
}
