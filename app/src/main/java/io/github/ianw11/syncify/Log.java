package io.github.ianw11.syncify;

/**
 * Created by Ian on 2/24/2017.
 */

public class Log {
    private static final String TAG = "SYNCIFY_LOG";

    private static SyncifyApplication mApplication;

    public static void initialize(SyncifyApplication application) {
        mApplication = application;
    }

    private static void emit(LogLevel logLevel, String tag, String message) {
        if (mApplication == null) {
            android.util.Log.e(TAG, "Unable to send log to server");
        } else {
            mApplication.getPlayerService().sendLog(logLevel, tag, message);
        }
    }

    public static void d(String tag, String message) {
        android.util.Log.d(tag, message);
        emit(LogLevel.DEBUG, tag, message);
    }

    public static void v(String tag, String message) {
        android.util.Log.v(tag, message);
        //emit(LogLevel.VERBOSE, tag, message);
    }

    public static void i(String tag, String message) {
        i(tag, message, true);
    }

    public static void i(String tag, String message, boolean sendToServer) {
        android.util.Log.i(tag, message);
        if (sendToServer) {
            emit(LogLevel.INFO, tag, message);
        }
    }

    public static void w(String tag, String message) {
        w(tag, message, true);
    }

    public static void w(String tag, String message, boolean sendToServer) {
        android.util.Log.w(tag, message);
        if (sendToServer) {
            emit(LogLevel.WARN, tag, message);
        }
    }

    public static void e(String tag, String message) {
        e(tag, message, true);
    }

    public static void e(String tag, String message, boolean sendToServer) {
        android.util.Log.e(tag, message);
        if (sendToServer) {
            emit(LogLevel.ERROR, tag, message);
        }
    }

    public enum LogLevel {
        DEBUG("D"),
        VERBOSE("V"),
        INFO("I"),
        WARN("W"),
        ERROR("E"),
        ;

        private final String mLabel;
        LogLevel(String label) {
            mLabel = label;
        }

        public String label() {
            return mLabel;
        }
    }
}
