package io.github.ianw11.syncify.musicsession;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.github.ianw11.syncify.R;
import io.github.ianw11.syncify.SyncifyApplication;
import io.github.ianw11.syncify.models.Session;
import io.github.ianw11.syncify.models.SessionDataModel;

/**
 * Created by Ian on 3/2/2017.
 */

public class RoomInfoDialog extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_room_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(false);

        final TextView roomName = (TextView) view.findViewById(R.id.room_info_name);
        final TextView roomId = (TextView) view.findViewById(R.id.room_info_id);
        final TextView numInRoom = (TextView) view.findViewById(R.id.room_info_num_in_room);

        final SyncifyApplication app = ((SyncifyApplication)getContext().getApplicationContext());
        final Session session = app.getSessionDataModel().getCurrentSession();

        roomName.setText(session.name);
        roomId.setText("Room id: " + session.id);
        numInRoom.setText("Currently " + session.numInRoom + " in room");

        view.findViewById(R.id.room_info_dismiss).setOnClickListener((v) -> dismiss());
    }
}
