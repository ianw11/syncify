package io.github.ianw11.syncify.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.ArrayList;
import java.util.List;

import kaaes.spotify.webapi.android.models.ArtistSimple;

/**
 * Created by Ian on 1/16/2016.
 */
public class Song {
    public final String spotifyUri;
    public final String name;
    public final List<String> artistNames;

    public Song(String uri, String title, List<String> artistNames) {
        this.spotifyUri = uri;
        this.name = title;
        this.artistNames = artistNames;
    }

    public Song(String uri, String name, ArtistSimple[] artists) {
        this.spotifyUri = uri;
        this.name = name;
        artistNames = new ArrayList<>();
        for (final ArtistSimple artist : artists) {
            artistNames.add(artist.name);
        }
    }

    public int numArtists() {
        return artistNames.size();
    }

    public List<String> artistNames() {
        return artistNames;
    }

    public JsonObject toJson() {
        final JsonObject obj = new JsonObject();
        obj.add("title", new JsonPrimitive(name));
        obj.add("uri", new JsonPrimitive(spotifyUri));
        final JsonArray artists = new JsonArray();
        for (final String artist : artistNames) {
            artists.add(artist);
        }
        obj.add("artists", artists);
        return obj;
    }
}
